#include <iostream>
#include <chrono>
#include <thread>
#include <time.h>
using namespace std;

int main(){
    int n,windowSize;
    srand (time(0));
    using namespace std::this_thread; // sleep_for, sleep_until
    using namespace std::chrono;
    cout<<"Enter Number of Frame: ";
    cin>>n; //total frame yang akan dikirim
    cout<<"Enter Windows Size: ";
    cin>>windowSize; //ukuran windows size
    int i=0;
    int it;
    int flag=0;
    int total=0; //variabel untuk jumlah pengiriman total fram
    while(i<n+windowSize)
	{
		if(flag<windowSize) //untuk perulangan pengiriman frame di awal program
		{
            sleep_for(1s);
            cout<<"SENDER : Frame "<<i<<" is sent\n";
            total++;
            flag++;
            i++;
        }
        if(flag>windowSize-1) //setelah pengiriman sejumlah windows size yang pertama kali 
		{
            if( rand() % 10 < (rand() % 10) ) //melakukan rand untuk menentukan lost atau tidak pengiriman
			{
                sleep_for(2s);
                cout<<"(Acknowledgement "<<i-windowSize<<" lost)\n";
                cout<<"(Sender timeouts-->Resend the frame)\n";
            }
            else
			{
                sleep_for(1s);
                cout<<"RECEIVER : Frame "<<i-windowSize<<" received correctly\n";
                cout<<"(acknowledgement "<<i-windowSize<<" received)\n";
                if(i<n) //mengecek i apakah masih dalam jumlah total frame
				{
                    sleep_for(1s);
                    cout<<"SENDER : Frame "<<i<<" is sent\n";
                    total++;
                }
                i++;
                continue;
            }
            int j = i - windowSize;
            for(int x = 0; x<windowSize; x++) //melakukan perulangan pengiriman sejumlah windows size
			{
                if(j<n) //mengecek jika nilai j masih dalam jumlah total frame
				{
                    sleep_for(1s);
                    cout<<"SENDER : Frame "<<j<<" is sent\n";
                    total++;
                }
                j++;
            }
        }
    }
    cout<<endl;
    cout<<"All frames sent correctly, total frame sent : "<<total<<endl;
    return 0;
}
