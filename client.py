import socket

HOST = '127.0.0.1'  # Server hostname / ip address
PORT = 1025         # Port used by server

def connect(framing, winSizing):
    i=0
    flag=0
    total=0
    frame=int(framing)
    winSize=int(winSizing)
    
    file = open(r"glob_val.txt","r+")
    file.truncate(0)
    file.write(winSizing)
    file.close()

    print('winsize', winSize)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as soc:
        soc.connect((HOST, PORT))
  
        while True:

            if(flag < winSize): # untuk perulangan pengiriman frame di awal program
                if(i < frame):
                    print("Send Frame: Frame ", i)
                    soc.sendall(str(i).encode('utf-8'))
                    total = total + 1
                else:
                    soc.sendall('none'.encode('utf-8'))
                i = i + 1
            
            data = soc.recv(1024).decode('utf-8')
            if(data == 'stop'):
                break
            elif(data == '1'):
                print('Acknowledge Validated ', i-winSize)
            elif(data == '0'):
                print('Data Loss ', i-winSize)
                i = i - winSize
                continue
            elif(data != 'none'):
                print(data)

def main():
    frames = input("Enter number of frames: ")
    winSizes = input("Enter windows size: ")
    connect(frames, winSizes)

if __name__ == "__main__":
    main()