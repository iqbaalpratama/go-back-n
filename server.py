import socket
import time
from random import random

HOST = '127.0.0.1'  # Standard loop back interface address (localhost)
PORT = 1025

size = 0
arr = []

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as soc:
    soc.bind((HOST, PORT))
    soc.listen()
    conn, addr = soc.accept()
    with conn:
        print('Connected by', addr)
        
        file = open(r"glob_val.txt","r+")
        size = int(file.readline())
        file.close()
        
        print('winsize', size)

        while True:
            data = conn.recv(1024).decode('utf-8')

            time.sleep(1)
            if not data:
                break

            if(data != 'none'):
                arr.append(int(data))
 
            if(len(arr) == size or data == 'none'):
                if(len(arr) == 0):
                    conn.sendall('stop'.encode('utf-8'))
                if(random() < 0.4):
                    a = 0
                    if(len(arr) != 0):
                        print('Data Loss ', arr[0])
                    conn.sendall(str(a).encode('utf-8'))
                    arr.clear()
                else:
                    a = 1
                    print('Received Frame ', arr[0])
                    conn.sendall(str(a).encode('utf-8'))
                    if(len(arr) != 0):
                        arr.pop(0)
            else:
                conn.sendall(str('none').encode('utf-8'))

